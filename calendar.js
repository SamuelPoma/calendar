// CALENDAR EXERCISE
// Creare un calendario dinamico con le festività di una nazione, selezionabile da un menu a tendina. Partiamo dal gennaio 2017 dando la possibilità di cambiare mese, gestendo il caso in cui l’API non possa ritornare festività.
//
// Ogni volta che cambio mese dovrò:
// Controllare se il mese è nel futuro
// Controllare quanti giorni ha il mese scelto formando così una lista
// Chiedere all’api quali sono le festività per il mese scelto
// Evidenziare le festività nella lista
//
// 	     Gennaio 2017
// <- Precedente      	Successivo ->
//
// 1 Gennaio - Capodanno
// 2 Gennaio
// 3 Gennaio
// 4 Gennaio
// 5 Gennaio
// 6 Gennaio - Epifania
// ...
//

$(document).ready(function(){
  counterMonth = 1;
  counterYears = 2017;
    $('#btn-countries').click(function() {
      updateCalendar();
      $(".days-container").fadeIn(2500);
      $("#container-calendar").show();
      $(".leaf-bottom-left").fadeIn(3500);
      $(".leaf-top-right").fadeIn(3500);
      $("#btn-left").fadeIn(3500);
      $("#btn-right").fadeIn(3500);
      var soundNature = new Audio("sounds/Sounds-of-nature.mp3");
      soundNature.play();
    });

    $("#btn-right").click(function(){
        counterMonth++
        if(counterMonth > 12) {
          counterMonth = 1;
          counterYears++
        }
        while (counterYears == 2018) {
          alert("The calendar is valid until 2017!");
          counterYears = 2017;
          counterMonth = 12;
        }
        console.log(counterMonth);
        updateCalendar();
    });

    $("#btn-left").click(function(){
        counterMonth--
        if(counterMonth < 1) {
          counterMonth = 12;
          counterYears--
        }
        console.log(counterMonth);
        updateCalendar();
    });

    function updateCalendar() {
      var country = $("#countries").val();
      $.ajax({
        url:"https://holidayapi.com/v1/holidays",
        method:"GET",
        data:{
          key:'616919fd-f3f5-44cf-8c42-17288d0d9178',
          country: country,
          year: counterYears,
          month: counterMonth,
        },
        success:function(data) {
          $(".container-date").html("");
          $(".select-month").html("");
          // console.log(data);
          // creo un oggetto moment mese gennaio 2017
          var month = moment("2017-" +  counterMonth)
          // torno il mese da number a stringa in lettere
          var monthLetter = moment("2017-" + counterMonth).format("MMMM");
          // console.log(januaryLetter);
          // trovo quanti giorni ha gennaio, mi ritorna un numero
          var daysMonth = month.daysInMonth();
          // prendo il primo giorno del mese in lettere

          // genero i 31 giorni del mese di gennaio
          for (var i = 1; i < daysMonth + 1 ; i++) {
            var daysInLetter = moment(counterYears + "-" + counterMonth + "-" + i).format('dddd');
            console.log(daysInLetter);
             var day = moment(counterYears + "-"  + counterMonth + "-" + i);
            $(".container-date").append('<div class = "item-date" dateymd="'+ day.format('YYYY-MM-DD')+'">' + i + " " + monthLetter + "<br>" + '<span class = "days-letter">' + daysInLetter + '</span>' + '</div>');
          }
          // GENERO AUTOMATICAMENTE LA PRIMA INTESTAZIONE DEL CALENDARIO NEL FORMATO MESE/ANNO
          // var year = moment(counterYears).format("YYYY");
          $(".select-month").append('<h3>' + monthLetter + " " + counterYears + "</h3>");
          // prendo le date delle due festività nel mese di gennaio contenute nell'APIS
          for (var i = 0; i < data.holidays.length; i++) {
            var holidayYmd = data.holidays[i].date;
            $(".item-date").each(function(){
                if ($(this).attr('dateymd')== holidayYmd) {
                  $(this).addClass("holiday");
                }
            });
          }
        }
      });
    }
});
